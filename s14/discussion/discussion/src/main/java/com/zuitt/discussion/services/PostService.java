package com.zuitt.discussion.services;

import com.zuitt.discussion.models.Post;
import com.zuitt.discussion.repositories.PostRepository;
import org.apache.coyote.Response;
import org.springframework.http.ResponseEntity;

public interface PostService {


    void createPost(String stringToken, Post post);
    Iterable<Post> getPosts();
    ResponseEntity deletePost(Long id, String stringToken);

    ResponseEntity updatePost(Long id, String stringToken,Post post);

    // to get all of a specific user

    Iterable<Post> getMyPosts(String stringToken);
}
